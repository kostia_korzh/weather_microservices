package me.study.forecast_service.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Forecast {

    private String city;
    private Integer temperature;
    private String description;

}
