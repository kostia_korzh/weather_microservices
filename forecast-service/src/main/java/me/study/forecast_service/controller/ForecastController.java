package me.study.forecast_service.controller;

import lombok.RequiredArgsConstructor;
import me.study.forecast_service.service.ForecastService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RefreshScope
@RestController
public class ForecastController {

    private final ForecastService forecastService;

    @Value("${message.text}")
    private String message;

    @RequestMapping("/api/test")
    public String test() {
        return message;
    }

    @GetMapping("/forecast/{city}")
    public String getForecastForLocation(@PathVariable String city) {
        return forecastService.getForecastForLocation(city);
    }

}
