package me.study.forecast_service.rabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

@RabbitListener(queues = "forecastServiceQueueResponse")
public class Handler {

    @RabbitHandler
    public void receive(String message) {
        System.out.println("--- Received message: " + message + " ---");
    }

}

