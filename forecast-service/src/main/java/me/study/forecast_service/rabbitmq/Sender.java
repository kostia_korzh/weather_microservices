package me.study.forecast_service.rabbitmq;

import com.rabbitmq.client.Delivery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

@Slf4j
@RabbitListener(queues = "forecastServiceQueueResponse")
public class Sender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

//    @Autowired
//    private DirectExchange directExchange;

//    @RabbitHandler
    public void sendForecastData(String city) {
        log.info("sending request to weather-api-service...");
        rabbitTemplate.convertAndSend("forecastServiceQueueRequest", city);
    }

}
