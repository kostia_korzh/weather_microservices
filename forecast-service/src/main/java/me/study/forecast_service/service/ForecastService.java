package me.study.forecast_service.service;

import lombok.RequiredArgsConstructor;
import me.study.forecast_service.model.Forecast;
import me.study.forecast_service.rabbitmq.Handler;
import me.study.forecast_service.rabbitmq.Sender;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class ForecastService {

    @Autowired
    private Sender sender;

    @Autowired
    private Handler handler;

    private final RabbitTemplate template;

    public String getForecastForLocation(String city) {

        sender.sendForecastData(city);

        String response = String.valueOf(template.receiveAndConvert("forecastServiceQueueResponse"));
        System.out.println(response);
        return response;

//        Integer temperature = 10;
//        return Forecast.builder()
//                .city(city)
//                .temperature(temperature)
//                .description("desc")
//                .build();
    }

}
