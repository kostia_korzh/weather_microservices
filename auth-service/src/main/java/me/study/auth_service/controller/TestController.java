package me.study.auth_service.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping(value = "/test", produces = {"application/JSON"})
    public String getTest() {
        return "Testing auth server as resource server...";
    }

}
