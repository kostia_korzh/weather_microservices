package me.study.weather_api_service.util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import me.study.weather_api_service.model.City;
import me.study.weather_api_service.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.Scanner;

@Component
public class Parser {

    private Gson gson;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    public Parser(Gson gson) {
        this.gson = gson;
    }

    public void parseCitiesFileToDb() {
        File file = new File("weather-api-service/src/main/resources/cities");
        try {
            Scanner scanner = new Scanner(file);
            StringBuilder wholeFile = new StringBuilder();
            while (scanner.hasNext()) {
                System.out.println("Executing parsing ...");
                wholeFile.append(scanner.nextLine()).append('\n');
            }
//            System.out.println(wholeFile);
            JsonArray jsonArray = gson.fromJson(String.valueOf(wholeFile), JsonArray.class);
            for (JsonElement element:jsonArray){
                City city = new City(element.getAsJsonObject().get("id").getAsLong(), element.getAsJsonObject().get("name").getAsString());
                cityRepository.save(city);
//                System.out.println(element.getAsJsonObject().get("id")+" : "+element.getAsJsonObject().get("name"));
            }
            System.out.println("Finishing parsing ...");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
