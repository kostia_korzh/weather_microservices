package me.study.weather_api_service;

import me.study.weather_api_service.repository.CityRepository;
import me.study.weather_api_service.util.Parser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

public class DBFiller implements CommandLineRunner {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private Parser parser;

    @Override
    public void run(String... args) throws Exception {
        if (cityRepository.findAll().size() == 0) {
            parser.parseCitiesFileToDb();
        }
    }
}
