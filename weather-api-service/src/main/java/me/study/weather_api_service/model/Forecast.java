package me.study.weather_api_service.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Forecast {

    private String city;
    private Integer temperature;
    private String description;

}
