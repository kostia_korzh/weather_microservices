package me.study.weather_api_service.rabbitmq;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean
    public Queue forecastServiceQueueRequest(){
        return new Queue("forecastServiceQueueRequest");
    }

    @Bean
    public Queue forecastServiceQueueResponse(){
        return new Queue("forecastServiceQueueResponse");
    }

    @Bean
    public Sender sender(){
        return new Sender();
    }

    @Bean
    public Handler handler(){
        return new Handler();
    }

    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange("tut.direct");
    }

}





