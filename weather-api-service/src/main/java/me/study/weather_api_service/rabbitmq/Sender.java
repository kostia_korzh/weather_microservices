package me.study.weather_api_service.rabbitmq;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import lombok.extern.slf4j.Slf4j;
import me.study.weather_api_service.model.City;
import me.study.weather_api_service.repository.CityRepository;
import me.study.weather_api_service.util.Constants;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

@Slf4j
public class Sender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private DirectExchange directExchange;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private Gson gson;

    public void sendForecastData(String city) {
        log.info("sending forecast data from weather-api-service to forecast-service...");
//        rabbitTemplate.convertAndSend("forecastServiceQueue", "test");
        retrieveFromApi(city);
    }

    private void retrieveFromApi(String city) {
        StringBuilder forecastRequest = new StringBuilder(Constants.WEATHER_API);
//        log.error(city);
        try {
            Long cityId = cityRepository.findCityByCityNameIgnoreCase(city).orElseThrow(RuntimeException::new).getId();
            forecastRequest.append("id=").append(cityId).append("&units=metric")
                    .append("&appid=").append(Constants.API_ID);

            String responseFromApi = restTemplate.getForEntity(forecastRequest.toString(), String.class).toString().replace("<200,", "");
            int endOfBody = responseFromApi.indexOf("},[") + 1;

            String bodyFromApi = responseFromApi.substring(0, endOfBody);

            rabbitTemplate.convertAndSend("forecastServiceQueueResponse", bodyFromApi);
        } catch (RuntimeException e) {
            rabbitTemplate.convertAndSend("forecastServiceQueueResponse", "No such city");
        }


//            System.out.println(responseFromApi);
    }
}
