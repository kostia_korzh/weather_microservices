package me.study.weather_api_service.rabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

@RabbitListener(queues = "forecastServiceQueueRequest")
public class Handler {

    @Autowired
    private Sender sender;

    @RabbitHandler
    public void receive(String message) {
        System.out.println("--- Received message: " + message.toString() + " ---");
        sender.sendForecastData(message);
    }

}
